import React from 'react';
import MapViewDirections from 'react-native-maps-directions';

const Directions = ({destination, origin, onReady}) => (
  <MapViewDirections
    destination={destination}
    origin={origin}
    onReady={onReady}
    apikey={'AIzaSyARQSqvKmYxvc3kI95pSU2EEaS1e2daeOU'}
    strokeWidth={3}
    strokeColor={'#222'}
  />
);

export default Directions;
