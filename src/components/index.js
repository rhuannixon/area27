/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';

import Mapa from './Mapa';
import Cultos from './Cultos';
import Escala from './Escala';
import Icon from 'react-native-vector-icons/MaterialIcons';
Icon.loadFont();
export default class TabNavigator extends Component {
  render() {
    return createAppContainer(
      createMaterialBottomTabNavigator(
        {
          Mapa: {
            screen: Mapa,
            navigationOptions: {
              tabBarLabel: 'Igrejas',
              tabBarIcon: ({tintColor}) => (
                <Icon name="map" size={24} color={tintColor} />
              ),
            },
          },
          Cultos: {
            screen: Cultos,
            navigationOptions: {
              tabBarIcon: ({tintColor}) => (
                <Icon name="event" size={24} color={tintColor} />
              ),
            },
          },
          Escala: {
            screen: Escala,
            navigationOptions: {
              tabBarLabel: 'Escalas',
              tabBarIcon: ({tintColor}) => (
                <Icon name="list" size={24} color={tintColor} />
              ),
            },
          },
        },
        {
          initialRouteName: 'Mapa',
          tabBarOptions: {
            activeTintColor: '#f0edf6',
            inactiveTintColor: '#3e2465',
          },
          barStyle: {backgroundColor: '#694fad'},
        },
      ),
    );
  }
}
