import React, {Component} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

class Cultos extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Cutos</Text>
      </View>
    );
  }
}
export default Cultos;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
