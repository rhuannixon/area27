/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';

import Mapa from './src/pages/Mapa';
import Cultos from './src/pages/Cultos';
import Escala from './src/pages/Escala';
import Perfil from './src/pages/Perfil';
import Icon from 'react-native-vector-icons/MaterialIcons';
Icon.loadFont();
const TabNavigator = createMaterialBottomTabNavigator(
  {
    Mapa: {
      screen: Mapa,
      navigationOptions: {
        tabBarLabel: 'Igrejas',
        tabBarIcon: ({tintColor}) => (
          <Icon name="map" size={24} color={tintColor} />
        ),
      },
    },
    Cultos: {
      screen: Cultos,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="favorite" size={24} color={tintColor} />
        ),
      },
    },
    Escala: {
      screen: Escala,
      navigationOptions: {
        tabBarLabel: 'Escalas',
        tabBarIcon: ({tintColor}) => (
          <Icon name="event-note" size={24} color={tintColor} />
        ),
      },
    },
    Perfil: {
      screen: Perfil,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="person" size={24} color={tintColor} />
        ),
      },
    },
  },
  {
    initialRouteName: 'Mapa',
    tabBarOptions: {
      activeTintColor: '#f0edf6',
      inactiveTintColor: '#3e2465',
    },
    barStyle: {backgroundColor: '#694fad'},
  },
);

const Tab = createAppContainer(TabNavigator);
export default Tab;
